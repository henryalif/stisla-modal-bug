<?php

namespace Database\Seeders;

use App\Models\Balance;
use Illuminate\Database\Seeder;

class BalanceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Balance::create([
            "user_id" => 1,
            "balance" => 90000
        ]);

        Balance::create([
            "user_id" => 2,
            "balance" => 60000
        ]);

        Balance::create([
            "user_id" => 3,
            "balance" => 1000
        ]);

        Balance::create([
            "user_id" => 4,
            "balance" => 0
        ]);

    }
}
