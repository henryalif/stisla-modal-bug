<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::create([
            "name" => "Pizza",
            "price" => "90000",
            "desc" => "A great Taste",
            "stock" => "100"
        ]);

        Product::create([
            "name" => "Burger",
            "price" => "35000",
            "desc" => "A great Burger",
            "stock" => "100"
        ]);

        Product::create([
            "name" => "Hotdog",
            "price" => "90000",
            "desc" => "A great Hotdog",
            "stock" => "100"
        ]);

        Product::create([
            "name" => "Lemon Tea",
            "price" => "90000",
            "desc" => "A great Lemon Tea",
            "stock" => "100"
        ]);

        Product::create([
            "name" => "Strawberry Ice",
            "price" => "90000",
            "desc" => "A great Strawberry Ice",
            "stock" => "100"
        ]);
    }
}
