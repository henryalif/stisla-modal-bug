<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\BankController;
use App\Http\Controllers\ShopController;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::get('/home', [HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/blank', function () {
    return view('blank');
});

Route::prefix('bank')->group(function () {
    Route::get('/', [BankController::class, 'index'])->name('bank');

    Route::get('/admin', [BankController::class, 'showingRequest'])->name('topup');
    Route::post('/topup', [BankController::class, 'create'])->name('topup');
    Route::get('/topup/approval/{transaction_id}', [BankController::class, 'topupApproval'])->name('topup.approval');
    Route::get('/topup/rejected/{transaction_id}', [BankController::class, 'topupRejected'])->name('topup.rejected');
});

Route::prefix('product')->group(function () {
    Route::get('/', [ShopController::class, 'index'])->name('transaction');
    Route::post('/addToCart/{id}', [ShopController::class, 'addtocart'])->name('addtocart');
    Route::get('/checkout-cart', [ShopController::class, 'checkoutcart'])->name('checkout-cart');
    Route::get('/checkout-page', [ShopController::class, 'checkoutpage'])->name('checkout-page');
    Route::get('/pay', [ShopController::class, 'pay'])->name('pay');

    Route::get('/canteen-approval/{invoice_id}', [ShopController::class, 'canteenApproval'])->name('canteen-approval');
    Route::get('/canteen-rejected/{invoice_id}', [ShopController::class, 'canteenRejected'])->name('canteen-approval');
    Route::get('/approval', [ShopController::class, 'sellerPage'])->name('approval');

    Route::get('/admin', [ShopController::class, 'productShow'])->name('admin');
    Route::get('/product/add', [ShopController::class, 'addProduct'])->name('product.add');
    Route::get('/product/edit/{id}', [ShopController::class, 'editProduct'])->name('product.edit');
    Route::get('/product/delete/{id}', [ShopController::class, 'deleteProduct'])->name('product.delete');

});

Route::get('/history', function () {
    return view('history.index');
});

Route::prefix("/user")->group( function() {
    Route::get('/', function(){
        return view('user');
    });
});
