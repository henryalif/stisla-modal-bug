<?php

namespace App\Http\Controllers;

use App\Models\Balance;
use App\Models\Product;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx\Rels;

class ShopController extends Controller
{
    public function index()
    {
        $products = Product::all();
        $carts = Transaction::where("user_id", Auth::user()->id)->where("status" , 1)->where("type" , 2)->get();
        $checkouts = Transaction::where("user_id", Auth::user()->id)->where("status" , 2)->where("type" , 2)->get();
        $balances = Balance::where("user_id", Auth::user()->id)->first();

        $total_cart = 0;
        $total_checkout = 0;

        foreach($carts as $cart){
            $total_cart +=($cart->product->price * $cart->qty);
        };

        foreach($checkouts as $checkout){
            $total_checkout +=($checkout->product->price * $checkout->qty);
        };


        return view("product.index",
        [
            "products"          => $products,
            "carts"             => $carts,
            "checkouts"         => $checkouts,
            "balances"          => $balances,
            "total_cart"        => $total_cart,
            "total_checkout"    => $total_checkout
        ],
        // compact('products', 'carts', 'checkouts', 'balances')
        );
    }

    public function addtocart(Request $request)
    {
        Transaction::create([
            "user_id" => Auth::user()->id,
            "product_id" => $request->product_id,
            "status" => 1,
            "qty" => $request->qty,
            "type" => 2
        ]);

        return redirect()->back()->with("status", "Success Add To Cart!");
    }

    public function checkoutcart()
    {
        $invoice_id = "INV_" . Auth::user()->id . now()->timestamp;

        Transaction::where("user_id", Auth::user()->id)->where("type", 2)->where("status", 1)->update([
            "invoice_id" => $invoice_id,
            "status" => 2
        ]);

        return redirect()->back()->with("status", "Checkout Successfully");
    }

    public function checkoutpage()
    {
        $checkouts = Transaction::where("user_id", Auth::user()->id)->where("status" , 2)->where("type" , 2)->get();

        $total_checkout = 0;

        foreach($checkouts as $checkout){
            $total_checkout +=($checkout->product->price * $checkout->qty);
        };

        return view("product.checkout",[
            "checkouts" => $checkouts,
            "total_checkout" => $total_checkout
        ]);
    }

    public function pay()
    {
        $datas = Transaction::where("user_id", Auth::user()->id)->where("type", 2)->where("status", 2);

        $total_data = 0;

        foreach($datas->get() as $data){
            $total_data += ($data->product->price * $data->qty);
        }

        $balances = Balance::where("user_id", Auth::user()->id)->first();

        $balances->update([
            "balance" => $balances->balance - $total_data
        ]);

        $datas->update([
            "status" => 3
        ]);

        return redirect()->back()->with("status", "Payment Success, Waiting Seller Approval");
    }

    public function canteenApproval($invoice_id)
    {
        $transactions = Transaction::where("invoice_id", $invoice_id);

        $total_data = 0;

        foreach($transactions->get() as $transaction){
            $total_data += ($transaction->qty * $transaction->product->price);
        }

        $transactions->update([
            "status" => 4 //transaction finished
        ]);

        return redirect()->back()->with("status", "Shopping Approved!");
    }

    public function canteenRejected($invoice_id)
    {
        $transactions = Transaction::where("invoice_id", $invoice_id);

        $total_data = 0;

        foreach($transactions->get() as $transaction){
            $total_data += ($transaction->qty * $transaction->product->price);
        }

        $balances = Balance::where("user_id", $transactions->get()[0]->user_id)->first();

        $balances->update([
            "balances" => $balances->balance + $total_data
        ]);

        $transactions->update([
            "status" => 5 //transaction rejected
        ]);
    }

    public function sellerPage()
    {
        // $shopping = Transaction::where("type", 1)->where("status", 2)->get();

        $shopping_submission = Transaction::where("type", 2);
        $shopping_by_invoice = Transaction::where("type", 2)->groupBy("invoice_id")->get();

        // dd($shopping_by_invoice);

        return view("product.approval", [
            "shopping_by_invoice" => $shopping_by_invoice,
            "shopping_submission" => $shopping_submission,
        ]);
    }

    public function productShow()
    {
        $products = Product::all();

        return view("product.admin", [
            "products" => $products
        ]);
    }

    public function addProduct(Request $request)
    {
        Product::create($request->all());
        return redirect()->back()->with("status", "Success add new product!");
    }

    public function editProduct(Request $request, $id)
    {
        Product::find($id)->update($request->all());
        return redirect()->back()->with("status", "Success edit product!");
    }

    public function deleteProduct($id)
    {
        Product::find($id)->delete();
        return redirect()->back()->with("status", "Success delete product!");
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
