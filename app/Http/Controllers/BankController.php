<?php

namespace App\Http\Controllers;

use App\Models\Balance;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BankController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $balances = Balance::where("user_id", Auth::user()->id)->first();
        // dd($balances);
        return view("bank.index",
        [
            "balances" => $balances,
        ]);
    }

    public function showingRequest()
    {
        $topuprequest = Transaction::where("type", 1)->where("status", 2)->get();

        return view('bank.admin', [
            "topuprequest" => $topuprequest
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if($request->type == 1){
            $invoice_id = "TOP_" . Auth::user()->id . now()->timestamp;

            Transaction::create([
                "user_id"       => Auth::user()->id,
                "qty"           => $request->qty,
                "invoice_id"    => $request->invoice_id,
                "type"          => $request->type,
                "status"        => 2
            ]);
        }

        return redirect()->back()->with("status", "Top Up Balance Proccessed!");
    }

    public function topupApproval($transaction_id)
    {
        $transaction = Transaction::find($transaction_id);

        $balances = Balance::where("user_id", $transaction->user_id)->first();

        Balance::where("user_id", $transaction->user_id)->update([
            "balance" => $balances->balance + $transaction->qty
        ]);

        $transaction->update([
            "status" => 3
        ]);

        return redirect()->back()->with("status", "Top Up Approved!");
    }

    public function topupRejected($transaction_id)
    {
        $transaction = Transaction::find($transaction_id);

        $transaction->delete();

        return redirect()->back()->with("status", "Top Up Rejected!");
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
