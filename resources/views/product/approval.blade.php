@extends('layouts.app')

@section('content')
    <section class="section">
        <div class="section-header">
            <h3 class="page__heading">Canteen Approval</h3>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <table id="example" class="table table-striped" style="width:100%">
                            <thead class="text-center">
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Invoice</th>
                                    <th>Status</th>
                                    <th>Detail</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody class="text-center">
                                @foreach ($shopping_by_invoice as $item)
                                @if ($item->status == 2 || $item->status == 3)
                                    <tr>
                                        <td>1</td>
                                        <td><span class="badge badge-dark">{{ $item->user->name }}</div></td>
                                        <td><span class="badge badge-info"><strong>{{ $item->invoice_id }}</strong></span></td>
                                        <td>{{ $item->status == 2 ? "Pending" : "Completed" }}</td>
                                        <td>
                                            <!-- Button trigger modal -->
                                            <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#testing">
                                                Add User
                                            </button>

                                            <div class="modal fade" id="testing" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="false">
                                                <div class="modal-dialog" style="z-index: 1100;">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Add User</h5>
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <form method="POST" action="{{ route('register') }}">
                                                            @csrf
                                                                <div class="form-group">
                                                                    <label>Name</label>
                                                                    <input type="text" class="form-control" name="name" value="{{ old("name") }}" id="name">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Email</label>
                                                                    <input type="text" class="form-control" name="email" value="{{ old("email") }}" id="email">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Role</label>
                                                                    <select class="form-select">
                                                                        <option selected>Choose...</option>
                                                                        <option value="1">Admin</option>
                                                                        <option value="2">Bank Teller</option>
                                                                        <option value="3">Canteen</option>
                                                                        <option value="4">Student</option>
                                                                      </select>
                                                                </div>
                                                                <div class="form-group">
                                                                    <input type="hidden" class="form-control" name="balance" value="0">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Password</label>
                                                                    <input type="text" class="form-control" name="password" value="{{ old("password") }}" id="password">
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                                                <button type="submit" class="btn btn-primary">Save changes</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <a class="btn btn-success btn-md" href="#" role="button"><i class="fas fa-check-square"></i></a>
                                            <a class="btn btn-danger btn-md" href="#" role="button"><i class="fas fa-times-circle"></i></a>
                                        </td>
                                    </tr>
                                @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
@endsection

@section('scripts')

<script>
    $(document).ready(function() {
    $('#example').DataTable();
    } );
</script>

@endsection
