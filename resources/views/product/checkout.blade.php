@extends('layouts.app')

@section('content')
    <section class="section">
        <div class="section-header">
            <h5>Balance : <strong>Rp. 50.000</strong></h5>
        </div>
        <div class="section-body">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif
            <div class="row d-flex justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        {{-- <div class="card-header">Checkout {{ count($carts) > 0 ? "#" . $carts[0]->invoice_id : "" }}</div> --}}
                        <div class="card-body" style="height: auto; overflow: auto">
                            <table id="example" class="table table-striped " style="width:100%">
                                <thead class="text-center">
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Qty</th>
                                        <th>Price</th>
                                        <th><strong>Total</strong></th>
                                    </tr>
                                </thead>
                                <tbody class="text-center">
                                    @foreach ($checkouts as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->product->name }}</td>
                                        <td><span class="badge badge-dark">{{ $item->qty }}</span></td>
                                        <td><strong>Rp. {{ number_format($item->product->price) }}</strong></td>
                                        <td><strong><span class="badge badge-primary">Rp. {{ number_format($item->product->price * $item->qty) }}</span></strong></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <br>
                            <button type="button" class="btn btn-block btn-md btn-outline-primary" disabled> Total Transaction : <strong>Rp. {{ number_format($total_checkout) }} </strong></button>
                            <a href="{{ route("pay") }}" class="btn btn-block btn-md btn-success">Pay!</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')

<script>
    $(document).ready(function() {
    $('#example').DataTable();
    } );
</script>

@endsection
